using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Conditions
{
    // Nombre de la condici�n, debe ser �nico ya que ser� localizado por �ste
    public string name;
    // Descripci�n de apoyo
    [TextArea]
    public string description;
    // Boolean para controlar si ha sido cumplida la condici�n
    public bool done;
}
