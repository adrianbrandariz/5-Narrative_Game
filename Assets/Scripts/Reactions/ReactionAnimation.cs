using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionAnimation : Reaction {
    // El animator del objeto que ser� animado
    public Animator target;
    // Nombre del trigger de la animaci�n a ejecutar
    public string triggerName;

    // M�todo que ejecuta la reacci�n con override para que pise el m�todo heredado
    protected override void React() {
        // Ejecuta la animaci�n
        target.SetTrigger(triggerName);
    }
}
