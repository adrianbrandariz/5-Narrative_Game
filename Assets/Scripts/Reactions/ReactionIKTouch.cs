using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionIKTouch : Reaction {
    // Punto de interacción de la IK
    public Transform interactionSpot;
    // Referencia al control de IK
    public IKInteract ikInteract;
    // IkGoal seleccionado para realizar la animación
    public AvatarIKGoal ikGoal;

    protected override void React() {
        if (delay == 0) {
            delay = ikInteract.movementDuration;
        } else {
            ikInteract.movementDuration = delay;
        }

        // Se asignan los parámetros del ikInteract
        ikInteract.target = interactionSpot;
        ikInteract.ikGoal = ikGoal;
        // Se inicia el ikInteract
        ikInteract.StartMovement();
    }
}
