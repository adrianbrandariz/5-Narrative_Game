using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Reaction : MonoBehaviour {

    #region Variables
    // Descripci�n de la reacci�n, como una nota para el editor, usando textArea para mejorar la visibilidad
    [TextArea]
    public string description;
    // Tiempo que tardar� en ejecutarse la reacci�n
    public float delay;
    private float delayCounter;
    // Referencia al interactable que ejecuta la reaction, se alimentar� de forma autom�tica
    // por lo que se ocultar� en el inspector
    [HideInInspector]
    public Interactable interactable;
    #endregion

    #region Unity Events
    // Update is called once per frame
    void Update() {

    }
    #endregion

    #region Methods
    /// <summary>
    /// Acci�n que se realizar� previo al delay
    /// </summary>
    protected virtual void React() {
        // C�digo de la reacci�n
    }

    /// <summary>
    /// Acci�n que se realizar� tras el delay
    /// </summary>
    protected virtual void PostReact() {
        // Una vez terminada la reaction, el interactable inicia la siguiente
        interactable.NextReaction();
    }

    /// <summary>
    /// Corrutina que ser� la encargada de ejecutar la reacci�n y el delay
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator DelayReact() {
        // Realiza las acciones de react
        React();

        // Se reinicia el contador del bucle
        delayCounter = delay;

        // Se realiza la espera por el tiempo indicado
        while (delayCounter > 0) {
            yield return new WaitForEndOfFrame();
            delayCounter -= Time.deltaTime;
        }

        PostReact();
    }


    public virtual void ExecuteReaction() {
        StartCoroutine(DelayReact());
    }
    #endregion
}
