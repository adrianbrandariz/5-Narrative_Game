using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para hacer uso del navmesh
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {
    #region Variables
    public Animator animator;
    public NavMeshAgent agent;
    // Layer sobre el que se pulsar� para que el personaje se desplace
    public LayerMask clickableLayer;
    // Margen a partir del cual se empieza a frenar
    public float slowingMargin = 1f;
    // Boolean que indica cuando el jugador puede o no moverse
    public bool canMove = true;

    // Interactable actual
    private Interactable currentInteractable;
    #endregion

    #region Unity Events
    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        Controls();
        AnimationVariables();
        SlowingStopping();
    }
    #endregion

    #region Methods

    private void Controls() {
        // Fire1 es el click izquierdo del rat�n
        if (Input.GetButtonDown("Fire1")) {
            Clicked();
        }
    }

    /// <summary>
    /// Alimenta las variables del Animator
    /// </summary>
    private void AnimationVariables() {
        if (!agent.pathPending) {
            /*
             * Se transforma la velocidad global del agent a una velocidad local para poder
             * alimentar al animator
             */
            Vector3 localVelocity = transform.InverseTransformDirection(agent.velocity);
            // Se indica al animator a que velocidad se mueve en el eje Z
            animator.SetFloat("SpeedForward", localVelocity.z);
            // Se indica al animator a que velocidad se mueve en el eje X
            animator.SetFloat("SpeedRight", localVelocity.x);
        }
    }

    /// <summary>
    /// Lanza un raycast y se mover� hacia el punto de impacto
    /// </summary>
    public void Clicked() {
        // Si el jugador no se puede mover se bloquea toda interacci�n
        if (!canMove) return;

        // Se lanza un raycast desde la posici�n del cursor
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Se crea una variable para almacenar el resultado del impacto
        RaycastHit groundHit;
        // Se realiza un raycast para identificar la direcci�n a la que se desplazar� el usuario
        if (Physics.Raycast(camRay, out groundHit, 100f, clickableLayer)) {
            // Se comprueba si el objeto impactado tiene un componente Interactable
            currentInteractable = groundHit.collider.GetComponent<Interactable>();

            // Si tiene un interactable
            if (currentInteractable) {
                // Se inicia el desplazamiento hasta el punto de interacci�n
                MoveTo(currentInteractable.interactionLocation.position);
            } else {
                // Se inicia el desplazamiento hacia el punto de impacto
                MoveTo(groundHit.point);
            }
        }
    }

    public void MoveTo(Vector3 destination) {
        // Se fija el destino en el navmesh agent
        agent.SetDestination(destination);
        // Se inicia el desplazamiento
        agent.isStopped = false;
        // Se permite la rotaci�n autom�tica al iniciar el movimiento
        agent.updateRotation = true;
    }

    /// <summary>
    /// Realiza las acciones de aproximaci�n y parada en el destino
    /// </summary>
    public void SlowingStopping() {
        if (agent.pathPending) return;

        // Si no hay interactable fijado no se hace nada
        if (!currentInteractable) return;
        // Cuando se entra en el margen de frenada y giro para posicionar al player
        if (agent.remainingDistance <= slowingMargin) {
            // Se para la rotaci�n autom�tica
            agent.updateRotation = false;

            // Se recupera la rotaci�n final del objetivo de interacci�n
            Quaternion targetRotation = currentInteractable.interactionLocation.rotation;

            transform.rotation = Quaternion.Lerp(transform.rotation,
                                                targetRotation,
                                                1f - agent.remainingDistance / slowingMargin);
        }

        // Cuando el player llega a su destino
        if (agent.remainingDistance <= agent.stoppingDistance) {
            // Se inicia la interacci�n
            currentInteractable.Interact(this);
            // Se libera el interactable a la espera de otra interacci�n
            currentInteractable = null;
        }
    }
    #endregion
}
