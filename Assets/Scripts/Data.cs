using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Data
{
    // Nombre de la escena actual
    public string currentScene;
    // Nombre del punto de entrada a la escena
    public string entrancePosition;
    // Array con el estado de todas las condiciones
    public Conditions[] allConditions;
}
