using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
    #region Variables
    // Transform que indicar� posici�n y rotaci�n a la que el jugador mirar� al realizar la interacci�n
    public Transform interactionLocation;

    // Array de condiciones que se deben cumplir para acceder a las reacciones positivas
    [Header("Conditions")]
    public string[] conditions;

    // Bloquea al jugador para que no se pueda mover durante la interacci�n
    public bool lockPlayer = true;
    // Jugador que est� realizando la interacci�n
    private PlayerMovement interactingPlayer;

    // Objeto padre de las reacciones positivas
    private Transform positiveReactions;
    // Objeto padre de las reacciones por defecto
    private Transform defaultReactions;

    // Cola para gestionar las reacciones a realizar
    private Queue<Reaction> reactionQueue = new Queue<Reaction>();
    #endregion

    #region Unity Events
    private void Start() {
        // Se recupera la referencia al objeto que contendr� las reacciones positivas
        positiveReactions = transform.Find("PositiveReactions");
        // Se recupera la referencia al objeto que contendr� las reacciones por defecto
        defaultReactions = transform.Find("DefaultReactions");
    }

    private void OnDisable() {
        // Si se desactiva el objeto, se libera al jugador
        if (interactingPlayer) {
            interactingPlayer.canMove = true;
            interactingPlayer = null;
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Gestionar� las condiciones y las reacciones de cada interacci�n
    /// </summary>
    public void Interact(PlayerMovement player) {
        // Por defecto se marca como cumplidas las condiciones
        bool success = true;
        // Se asigna el player que estar� interactuando
        interactingPlayer = player;

        // Se recorren todas las condiciones del Interactable
        foreach (string condition in conditions) {
            // Se comprueba si no se cumple la condici�n
            if (!DataManager.instance.CheckCondition(condition)) {
                // Si no se cumple se modifica el valor de la variable success
                success = false;
                // Si no se cumple se rombe el bucle
                break;
            }
        }

        // Si se cumplen las condiciones y el n�mero de condiciones es mayor que 0
        if (success && conditions.Length > 0) {
            // Se vuelca la lista de reacciones positivas
            QueueReactions(positiveReactions);
        } else {
            QueueReactions(defaultReactions);
        }

        // Una vez preparada la cola se ejecuta la primera reacci�n
        NextReaction();
    }

    /// <summary>
    /// Pone en cola todas las reacciones que contenga el transform recibido como par�metro
    /// </summary>
    /// <param name="reactionContainer"></param>
    private void QueueReactions(Transform reactionContainer) {
        // Se limpian las acciones previas que pudieran quedar acumuladas
        reactionQueue.Clear();

        foreach (Reaction reaction in reactionContainer.GetComponentsInChildren<Reaction>()) {
            // Se indica a la reacci�n que este ser� el interactable que la gestione
            reaction.interactable = this;
            // Se introduce en la cola la reacci�n
            reactionQueue.Enqueue(reaction);
        }
    }

    public void NextReaction() {
        if (reactionQueue.Count > 0) {
            // Si el interactable ha solicitado bloquear al jugador se impide que se pueda mover
            interactingPlayer.canMove = !lockPlayer;
            // Se recupera el siguiente elemento y lo ejecuta
            Reaction currentReaction;
            currentReaction = reactionQueue.Dequeue();
            currentReaction.ExecuteReaction();
        } else {
            // Si no quedan reacciones se libera al jugador
            interactingPlayer.canMove = true;
            // Se elimina su referencia
            interactingPlayer = null;
        }
    }
    #endregion
}
