using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKInteract : MonoBehaviour {
    #region Variables
    public Animator animator;
    // Duraci�n del movimiento de interacci�n
    public float movementDuration = 1f;
    // Contador de tiempo de interacci�n
    private float movementTimer;
    // Interpolaci�n de 0 a 1 para la curva de animaci�n
    private float interpolation;
    // Curva de animaci�n para definir el peso del IK
    public AnimationCurve curve;
    // Objetivo del IK
    public Transform target = null;
    // El IKGoal del avatar, 4 extremidades disponibles
    public AvatarIKGoal ikGoal;
    #endregion

    #region Unity Events
    // Update is called once per frame
    void Update() {
        if (movementTimer > 0) {
            movementTimer -= Time.deltaTime;
            // Se eval�a la curva en funci�n del tiempo de animaci�n transcurrido
            interpolation = 1 - (movementTimer / movementDuration);
        }
    }

    private void OnAnimatorIK(int layerIndex) {
        // Si no hay definido animator o no hay un target, no se hace nada
        if (!animator || !target) return;

        // Peso del ik de la cabeza del player, mirando hacia el objetivo en funci�n del peso de la animaci�n
        animator.SetLookAtWeight(curve.Evaluate(interpolation));
        // Se indica la direcci�n a la que mirar� la cabeza del player
        animator.SetLookAtPosition(target.position);

        // Se define el peso de la posici�n para el movimiento del IK
        animator.SetIKPositionWeight(ikGoal, curve.Evaluate(interpolation));
        animator.SetIKPosition(ikGoal, target.position);
        // Se define peso de la rotaci�n para el movimiento de la IK
        animator.SetIKRotationWeight(ikGoal, curve.Evaluate(interpolation));
        animator.SetIKRotation(ikGoal, target.rotation);
    }
    #endregion

    #region Methods
    /// <summary>
    /// Inicia el movimiento del IK
    /// </summary>
    [ContextMenu("StartMovement")]
    public void StartMovement() {
        movementTimer = movementDuration;
    }
    #endregion
}
