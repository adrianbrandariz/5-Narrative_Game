using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKFootPlacement : MonoBehaviour {
    #region Variables
    // Animator para IK de las piernas
    public Animator animator;
    // Layer para identificar el suelo
    public LayerMask layerMask;

    [Range(0, 1f)]
    // Offset de distancia hasta el suelo
    public float groundOffset = 0.12f;

    [Range(0, 1f)]

    public float weight = 1f;
    #endregion

    #region Unity Events
    private void OnAnimatorIK(int layerIndex) {
        // Se fija el peso de los pies para tener en cuenta la IK
        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, weight);
        animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, weight);
        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, weight);
        animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, weight);
        // Se configuran los pies respecto al suelo
        GroundIK(AvatarIKGoal.LeftFoot);
        GroundIK(AvatarIKGoal.RightFoot);
    }
    #endregion

    #region Methods
    /// <summary>
    /// Aterriza el IK en el suelo para posicionar los pies
    /// </summary>
    /// <param name="goal"></param>
    private void GroundIK(AvatarIKGoal goal) {
        RaycastHit hit;

        /*
         * Se lanza el raycast con origen en una unidad por encima
         * de la posici�n del goal en direcci�n hacia abajo.
         */
        Ray ray = new Ray(animator.GetIKPosition(goal) + Vector3.up, Vector3.down);

        if (Physics.Raycast(ray, out hit, groundOffset + 1, layerMask)) {
            // Punto de impacto donde posicionar el pie
            Vector3 footPosition = hit.point;
            // Se tiene en cuenta el offset a la hora de posicionar el pie
            footPosition.y += groundOffset;
            // Se fija la posici�n en el objetivo de IK del avatar
            animator.SetIKPosition(goal, footPosition);

            // Orientaci�n correcta del pie
            Vector3 normalForward = Vector3.Cross(hit.normal, -transform.right);

            animator.SetIKRotation(goal, Quaternion.LookRotation(normalForward, hit.normal));
        }
    }
    #endregion
}
