using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para la gestión de la carga de escenas
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    #region Variables
    // Canvas utilizado para realizar el fundido a negro de pantalla
    public CanvasGroup faderCanvasGroup;
    // Duración del fundido
    public float fadeDuration = 1f;

    // Configuración inicial de escenaa y posición
    private string startingScene = "BlockingScene";
    private string initialStartingPosition = "ExteriorDoor";

    // Para controlar si se está realizando un fade
    private bool isFading;

    // SINGLETON
    public static SceneController instance;
    #endregion

    #region Unity Events
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    IEnumerator Start() {
        yield return null;
    }

    // Update is called once per frame
    void Update() {




    }
    #endregion

    #region Methods
    /// <summary>
    /// Realiza un fundido a visible o invisible
    /// </summary>
    /// <param name="finalAlpha"></param>
    /// <returns></returns>
    private IEnumerator Fade(float finalAlpha) {
        // Indicamos que se inicia el proceso de fundido
        isFading = true;
        // Mientras se realiza el fundido, bloqueamos posibles interacciones
        faderCanvasGroup.blocksRaycasts = true;

        // Se inicializa el contador de tiempo
        float timerCounter = fadeDuration;
        // Se recupera el alpha inicial del canvas
        float initialAlpha = faderCanvasGroup.alpha;

        while (timerCounter > 0) {
            // Se hace un lerp para realizar el fundido de forma gradual
            faderCanvasGroup.alpha = Mathf.Lerp(initialAlpha,
                                                finalAlpha,
                                                1f - timerCounter / fadeDuration);
            timerCounter -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // Por si el resultado no es el valor exacto se realiza esta asignación
        faderCanvasGroup.alpha = finalAlpha;
        // Se indica que ya se ha terminado el fade
        isFading = false;
        // Se retira el bloqueo de raycast al finalizar el fade
        faderCanvasGroup.blocksRaycasts = false;
    }

    /// <summary>
    /// Para testear si funciona bien el fade
    /// </summary>
    [ContextMenu("Fade Test")]
    public void FadeTest() {
        if(faderCanvasGroup.alpha == 0f) {
            StartCoroutine(Fade(1f));
        } else {
            StartCoroutine(Fade(0f));
        }
    }
    #endregion
}
