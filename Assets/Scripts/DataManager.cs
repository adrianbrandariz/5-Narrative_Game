using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder serializar en binario
using System.Runtime.Serialization.Formatters.Binary;
// Para poder leer/grabar en ficheros
using System.IO;
// Para poder filtrar los arrays
using System.Linq;

public class DataManager : MonoBehaviour {
    #region Variables
    // Objeto que contendrá toda la información de condiciones, items, etc... así como su estado
    public Data data;
    // Nombre del fichero de guardado
    public string fileName = "data.dat";
    // Combinación de ruta + nombre de fichero
    private string dataPath;

    // Instancia Singleton
    public static DataManager instance;
    #endregion

    #region Unity Events
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        // Para identificar rápidamente el dataPath
        Debug.Log(Application.persistentDataPath);

        dataPath = Application.persistentDataPath + "/" + fileName;

        // Carga de fichero
        Load();
    }
    #endregion

    #region Methods
    /// <summary>
    /// Realiza el guardado del data de forma persistente
    /// </summary>
    public void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dataPath);
        bf.Serialize(file, data);
        file.Close();
    }

    /// <summary>
    /// Realiza la carga de contenido almacenado de forma persistente (si existe)
    /// </summary>
    public void Load() {
        if (!File.Exists(dataPath)) return;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(dataPath, FileMode.Open);
        data = (Data)bf.Deserialize(file);
        file.Close();
    }

    /// <summary>
    /// Devuelve el estado en que se encuentra la condición recibida como parámetro
    /// </summary>
    /// <param name="conditionName"></param>
    /// <returns></returns>
    public bool CheckCondition(string conditionName) {
        // Se busca la condición en la lista de condiciones
        Conditions result = data.allConditions.Where(c => c.name == conditionName).FirstOrDefault();

        // Si la condición existe
        if (result != null) {
            // Se devuelve su estado
            return result.done;
        }

        Debug.LogWarning(conditionName + "La condición no existe y no se puede revisar");

        return false;
    }

    /// <summary>
    /// Cambia el estado de una condición al estado indicado
    /// </summary>
    /// <param name="conditionName"></param>
    /// <param name="done"></param>
    public void SetCondition(string conditionName, bool done) {
        // Se busca una condición que cumpla el criterio indicado
        Conditions result = data.allConditions.Where(c => c.name == conditionName).FirstOrDefault();

        // Si existe la condición
        if (result != null) {
            // Se modifica su estado asignando el indicado
            result.done = done;
        }
    }

    /// <summary>
    /// Elimina el fichero de guardado para resetear el estado
    /// </summary>
    [ContextMenu("Delete Save State")]
    public void DeleteSaveState() {
        // Si existe el fichero se borra
        if (File.Exists(dataPath)) File.Delete(dataPath);
    }
    #endregion
}
